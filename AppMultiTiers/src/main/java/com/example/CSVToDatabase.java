package com.example;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class CSVToDatabase {

    public void importCSVToDatabase(String csvFilePath) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.FRANCE);

        try (BufferedReader br = new BufferedReader(new FileReader(csvFilePath))) {
            String line;
            // Skip the header line
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                if (values.length < 7) {
                    continue; // Skip lines that don't have enough data
                }

                ObjetsTrouves objet = new ObjetsTrouves();
                // Parse and set date only if it is not empty
                if (!values[0].isEmpty()) {
                    objet.setDate(OffsetDateTime.parse(values[0].replaceAll("^\"|\"$", ""), formatter).toLocalDateTime());
                }
                if (!values[1].isEmpty()) {
                    objet.setDateRestitution(OffsetDateTime.parse(values[1].replaceAll("^\"|\"$", ""), formatter).toLocalDateTime());
                }
                objet.setGare(values[2]);
                objet.setCodeUIC(values[3]);
                objet.setNatureObjets(values[4]);
                objet.setTypeObjets(values[5]);
                objet.setTypeEnregistrement(values[6]);

                session.save(objet);
            }
            transaction.commit();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}