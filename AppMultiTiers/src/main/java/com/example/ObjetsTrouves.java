package com.example;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "ObjetsTrouves")
public class ObjetsTrouves {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime date;

    @Column(name = "date_restitution")
    private LocalDateTime dateRestitution;

    private String gare;

    @Column(name = "code_UIC")
    private String codeUIC;

    @Column(name = "nature_objets")
    private String natureObjets;

    @Column(name = "type_objets")
    private String typeObjets;

    @Column(name = "type_enregistrement")
    private String typeEnregistrement;

    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getDateRestitution() {
        return dateRestitution;
    }

    public void setDateRestitution(LocalDateTime dateRestitution) {
        this.dateRestitution = dateRestitution;
    }

    public String getGare() {
        return gare;
    }

    public void setGare(String gare) {
        this.gare = gare;
    }

    public String getCodeUIC() {
        return codeUIC;
    }

    public void setCodeUIC(String codeUIC) {
        this.codeUIC = codeUIC;
    }

    public String getNatureObjets() {
        return natureObjets;
    }

    public void setNatureObjets(String natureObjets) {
        this.natureObjets = natureObjets;
    }

    public String getTypeObjets() {
        return typeObjets;
    }

    public void setTypeObjets(String typeObjets) {
        this.typeObjets = typeObjets;
    }

    public String getTypeEnregistrement() {
        return typeEnregistrement;
    }

    public void setTypeEnregistrement(String typeEnregistrement) {
        this.typeEnregistrement = typeEnregistrement;
    }
}