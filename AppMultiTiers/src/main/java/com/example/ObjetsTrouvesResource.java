package com.example;

import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/objets")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ObjetsTrouvesResource {

    @GET
    public List<ObjetsTrouves> getAllObjets() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<ObjetsTrouves> objetsTrouvesList = session.createQuery("from ObjetsTrouves", ObjetsTrouves.class).list();
        session.close();
        return objetsTrouvesList;
    }
    
    @POST
    public Response createObjet(ObjetsTrouves objet) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(objet);
        transaction.commit();
        session.close();
        return Response.status(Response.Status.CREATED).entity(objet).build();
    }

    @GET
    @Path("/{id}")
    public Response getObjetById(@PathParam("id") Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        ObjetsTrouves objet = session.get(ObjetsTrouves.class, id);
        session.close();
        if (objet == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(objet).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateObjet(@PathParam("id") Long id, ObjetsTrouves updatedObjet) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        ObjetsTrouves existingObjet = session.get(ObjetsTrouves.class, id);
        if (existingObjet == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        existingObjet.setDate(updatedObjet.getDate());
        existingObjet.setDateRestitution(updatedObjet.getDateRestitution());
        existingObjet.setGare(updatedObjet.getGare());
        existingObjet.setCodeUIC(updatedObjet.getCodeUIC());
        existingObjet.setNatureObjets(updatedObjet.getNatureObjets());
        existingObjet.setTypeObjets(updatedObjet.getTypeObjets());
        existingObjet.setTypeEnregistrement(updatedObjet.getTypeEnregistrement());
        session.update(existingObjet);
        transaction.commit();
        session.close();
        return Response.ok(existingObjet).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteObjet(@PathParam("id") Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        ObjetsTrouves objet = session.get(ObjetsTrouves.class, id);
        if (objet == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        session.delete(objet);
        transaction.commit();
        session.close();
        return Response.noContent().build();
    }
}