package com.example;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppInitializer implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        CSVToDatabase csvToDatabase = new CSVToDatabase();
        csvToDatabase.importCSVToDatabase("/Users/dorian/Desktop/Rattrapage_Java/AppMultiTiers/objets-trouves-restitution.csv");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        // Cleanup code if needed
    }
}