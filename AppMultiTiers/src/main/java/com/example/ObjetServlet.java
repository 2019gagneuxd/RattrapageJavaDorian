package com.example;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.Transaction;

@WebServlet("/objet")
public class ObjetServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Affiche la page JSP
        request.getRequestDispatcher("/views/index.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dateStr = request.getParameter("date");
        String dateRestitutionStr = request.getParameter("date_restitution");
        String gare = request.getParameter("gare");
        String codeUIC = request.getParameter("code_UIC");
        String natureObjets = request.getParameter("nature_objets");
        String typeObjets = request.getParameter("type_objets");
        String typeEnregistrement = request.getParameter("type_enregistrement");

        // Convertir les dates à partir des chaînes
        LocalDateTime date = LocalDateTime.parse(dateStr, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        LocalDateTime dateRestitution = LocalDateTime.parse(dateRestitutionStr, DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        // Enregistrer l'objet en utilisant Hibernate
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        ObjetsTrouves objet = new ObjetsTrouves();
        objet.setDate(date);
        objet.setDateRestitution(dateRestitution);
        objet.setGare(gare);
        objet.setCodeUIC(codeUIC);
        objet.setNatureObjets(natureObjets);
        objet.setTypeObjets(typeObjets);
        objet.setTypeEnregistrement(typeEnregistrement);

        session.save(objet);
        transaction.commit();
        session.close();

        // Rediriger vers la page JSP
        response.sendRedirect(request.getContextPath() + "/objet");
    }
}