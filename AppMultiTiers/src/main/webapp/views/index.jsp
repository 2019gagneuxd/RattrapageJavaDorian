<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Objets Trouvés</title>
</head>
<body>
    <h1>Ajouter un Objet Trouvé</h1>
    <form action="${pageContext.request.contextPath}/objet" method="post">
        <label for="type">Type:</label>
        <input type="text" id="type" name="type"><br>
        <label for="description">Description:</label>
        <input type="text" id="description" name="description"><br>
        <label for="date">Date:</label>
        <input type="text" id="date" name="date"><br>
        <label for="location">Location:</label>
        <input type="text" id="location" name="location"><br>
        <input type="submit" value="Ajouter">
    </form>
</body>
</html>