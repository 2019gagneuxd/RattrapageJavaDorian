# swagger_client.DefaultApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**objects_get**](DefaultApi.md#objects_get) | **GET** /objects | Get lost items from a specific station
[**objects_post**](DefaultApi.md#objects_post) | **POST** /objects | Add a new lost item
[**unreturned_objects_get**](DefaultApi.md#unreturned_objects_get) | **GET** /unreturned_objects | Get unreturned items of a specific type

# **objects_get**
> list[LostItem] objects_get(station, limit)

Get lost items from a specific station

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
station = 'station_example' # str | Name of the station (gare)
limit = 56 # int | Number of items to retrieve (x)

try:
    # Get lost items from a specific station
    api_response = api_instance.objects_get(station, limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->objects_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **station** | **str**| Name of the station (gare) | 
 **limit** | **int**| Number of items to retrieve (x) | 

### Return type

[**list[LostItem]**](LostItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **objects_post**
> LostItem objects_post(body)

Add a new lost item

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
body = swagger_client.NewLostItem() # NewLostItem | 

try:
    # Add a new lost item
    api_response = api_instance.objects_post(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->objects_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**NewLostItem**](NewLostItem.md)|  | 

### Return type

[**LostItem**](LostItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **unreturned_objects_get**
> list[LostItem] unreturned_objects_get(type, limit)

Get unreturned items of a specific type

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
type = 'type_example' # str | Type of items (z)
limit = 56 # int | Number of items to retrieve (x)

try:
    # Get unreturned items of a specific type
    api_response = api_instance.unreturned_objects_get(type, limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->unreturned_objects_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| Type of items (z) | 
 **limit** | **int**| Number of items to retrieve (x) | 

### Return type

[**list[LostItem]**](LostItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

