package com.example.lostitems.service;

import com.example.lostitems.model.LostItem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class LostItemService {
    private SessionFactory factory;

    public LostItemService() {
        factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(LostItem.class)
                .buildSessionFactory();
    }

    public List<LostItem> getAllItems() {
        Session session = factory.getCurrentSession();
        Transaction tx = null;
        List<LostItem> items = null;

        try {
            tx = session.beginTransaction();
            items = session.createQuery("from LostItem", LostItem.class).getResultList();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return items;
    }

    public void addItem(LostItem item) {
        Session session = factory.getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(item);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void updateItem(int id, LostItem updatedItem) {
        Session session = factory.getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            LostItem item = session.get(LostItem.class, id);
            if (item != null) {
                item.setName(updatedItem.getName());
                item.setDescription(updatedItem.getDescription());
                item.setLocation(updatedItem.getLocation());
                item.setDateFound(updatedItem.getDateFound());
                item.setReturned(updatedItem.isReturned());
                session.update(item);
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void deleteItem(int id) {
        Session session = factory.getCurrentSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            LostItem item = session.get(LostItem.class, id);
            if (item != null) {
                session.delete(item);
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}