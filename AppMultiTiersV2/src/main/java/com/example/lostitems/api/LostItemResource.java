package com.example.lostitems.api;

import com.example.lostitems.model.LostItem;
import com.example.lostitems.service.LostItemService;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/objects")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LostItemResource {

    // Dependency injection of service class (to be implemented)
    private LostItemService lostItemService = new LostItemService();

    @GET
    public List<LostItem> getAllItems() {
        return lostItemService.getAllItems();
    }

    @POST
    public Response addItem(LostItem item) {
        lostItemService.addItem(item);
        return Response.status(Response.Status.CREATED).entity(item).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateItem(@PathParam("id") int id, LostItem item) {
        lostItemService.updateItem(id, item);
        return Response.ok().entity(item).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteItem(@PathParam("id") int id) {
        lostItemService.deleteItem(id);
        return Response.noContent().build();
    }
}