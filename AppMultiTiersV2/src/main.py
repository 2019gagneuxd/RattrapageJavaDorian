import requests
from swagger_client import ApiClient, Configuration, DefaultApi, LostItem
from datetime import datetime
import time

def format_datetime(date_str):
    try:
        dt = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S%z')
        # Format to a different pattern if necessary
        return dt.strftime('%Y-%m-%dT%H:%M:%SZ')  # ISO 8601 format with Zulu time
    except ValueError as e:
        print(f"Failed to parse date: {date_str}. Error: {e}")
        return None

# SNCF API client setup
SNCF_API_URL = "https://ressources.data.sncf.com/api/records/1.0/search/"
SNCF_DATASET_ID = "objets-trouves-restitution"

def get_lost_items_from_station(station, limit):
    params = {
        "dataset": SNCF_DATASET_ID,
        "rows": limit,
        "q": f"gc_obo_gare_origine_r_name={station}"
    }
    response = requests.get(SNCF_API_URL, params=params)
    
    if response.status_code == 200:
        json_response = response.json()
        return json_response.get('records', [])
    else:
        print("Failed to retrieve data from SNCF API:", response.content)
        return []

def get_unreturned_items_of_type(item_type, limit):
    params = {
        "dataset": SNCF_DATASET_ID,
        "rows": limit,
        "q": f"gc_obo_type_c={item_type} AND gc_obo_date_heure_restitution_c IS NULL"
    }
    response = requests.get(SNCF_API_URL, params=params)
    
    if response.status_code == 200:
        json_response = response.json()
        return json_response.get('records', [])
    else:
        print("Failed to retrieve data from SNCF API:", response.content)
        return []

def add_item_to_database(api_instance, item_data):
    try:
        item = LostItem(**item_data)
        api_instance.objects_post(item)  # Use the correct method here based on your API definition
        print("Item added successfully")
    except Exception as e:
        print("Failed to add item:", e)

def populate_database():
    # Setup configuration for local server API
    configuration = Configuration()
    configuration.host = "http://localhost:8080/AppMultiTiersV2/api"
    api_client = ApiClient(configuration=configuration)
    api_instance = DefaultApi(api_client)
    
    items_from_strasbourg = get_lost_items_from_station("Strasbourg", 80)
    unreturned_electronics = get_unreturned_items_of_type("Appareils électroniques, informatiques, appareils photo", 100)

    for item in items_from_strasbourg + unreturned_electronics:
        fields = item.get("fields", {})
        date_found_str = fields.get("date", "")
        formatted_date = format_datetime(date_found_str)
        print("Formatted date:", formatted_date)
        
        if formatted_date is None:
            continue  # Skip this item if the date format is incorrect
        
        item_data = {
            "name": fields.get("gc_obo_nature_c", ""),
            "location": fields.get("gc_obo_gare_origine_r_name", ""),
            "date_found": formatted_date,
            "returned": fields.get("gc_obo_date_heure_restitution_c") is not None
        }
        print("Sending item data to API:", item_data)  # Debugging: print the item data
        time.sleep(0.1)
        add_item_to_database(api_instance, item_data)

if __name__ == "__main__":
    populate_database()